# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "onlinestore"
app_title = "Online Store"
app_publisher = "ahmad ragheeb"
app_description = "My online store"
app_icon = "octicon octicon-file-directory"
app_color = "yellow"
app_email = "pq4@hotmail.co.uk"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/onlinestore/css/onlinestore.css"
# app_include_js = "/assets/onlinestore/js/onlinestore.js"

# include js, css files in header of web template
# web_include_css = "/assets/onlinestore/css/onlinestore.css"
# web_include_js = "/assets/onlinestore/js/onlinestore.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "onlinestore.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "onlinestore.install.before_install"
# after_install = "onlinestore.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "onlinestore.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events



#doc_events = {    "Sales Order": {        "after_insert": "onlinestore.www.shopcart.save_person"    }}



# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"onlinestore.tasks.all"
# 	],
# 	"daily": [
# 		"onlinestore.tasks.daily"
# 	],
# 	"hourly": [
# 		"onlinestore.tasks.hourly"
# 	],
# 	"weekly": [
# 		"onlinestore.tasks.weekly"
# 	]
# 	"monthly": [
# 		"onlinestore.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "onlinestore.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "onlinestore.event.get_events"
# }

