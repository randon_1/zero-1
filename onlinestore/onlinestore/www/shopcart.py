from __future__ import unicode_literals
import frappe
from frappe import throw, _
from frappe.model.document import Document
from frappe.auth import LoginManager


def get_context(context):
    context.list_items = frappe.get_list('Sales Order', '*')


@frappe.whitelist(allow_guest=True)
def addToShopcart():
    item = frappe.get_list("Item", "*", filters={"item_code": "02"})
    if item:

        price = "22"
        ss = frappe.get_doc({
            "doctype": "Sales Order",
            "delivery_date": "05 - 12 - 2016",
            "transaction_date": "05-12-2016",
            "customer": "Yussuf",
            "items": [{"item_code": item[0].item_code,
                       "item_name": item[0].item_name,
                       "description": item[0].description,
                       "price": price}]
        });
        ss.insert(ignore_permissions=True)
        return "done"
    else:
        msg = "No such item available"
        return msg

frappe.whitelist(allow_guest=True)
def save_person(doc,method):

    frappe.get_doc({"doctype":"Sales Order","delivery_date":"05 - 12 - 2016",  "transaction_date": "05-12-2016",
            "customer": "Yussuf",
            "items": [{"item_code": "01",
                       "item_name": "king",
                       "description":"blaaah",
                       "price":"22"}]}).insert();

